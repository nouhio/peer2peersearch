import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.List;
import java.util.Set;


import org.json.*;

public class RequestHandler implements Runnable{



	DatagramPacket packet;
	byte[] buffer = new byte[1024];
	PeerNode node;
	String text=null;


	public RequestHandler(DatagramPacket packet ,PeerNode node) throws Exception {
		this.packet = packet;
		this.node=node;
	}

	public void run() 
	{

		try{
			buffer = packet.getData();
			String request=new String(buffer);

			System.out.println(request);

			JSONObject jsonObj = new JSONObject(request);
			String requestType=jsonObj.get("type").toString();


			if(requestType.equals("JOINING_NETWORK_SIMPLIFIED")) {
				//System.out.println("Join Start");
				int node_id=(int)jsonObj.get("node_id");
				int sendPort=(int)jsonObj.get("sending_port");
				node.routing_Table.put(node_id, sendPort);
				handleJoin(sendPort);
			}


			else if(requestType.equals("UPDATE_TABLE")) {
				//System.out.println("Updating Table" + " for node " + node.node_id);
				String ids=jsonObj.get("node_id").toString();
				String ports=jsonObj.get("port").toString();

				String subIds= ids.substring(1,ids.length()-1);
				String subPorts= ports.substring(1,ports.length()-1);

				String [] id=subIds.split(",");
				String [] port=subPorts.split(",");

				for(int i=0;i<id.length;i++){
					if(node.node_id != Integer.parseInt(id[i])){
						node.routing_Table.put(Integer.parseInt(id[i]),Integer.parseInt(port[i]));
					}
				}

			}

			else if(requestType.equals("INDEX")) {

				int sendPort=(int)jsonObj.get("sendPort");
				String url=jsonObj.get("link").toString();
				String keyword=jsonObj.get("keyword").toString();

				handleIndexing(sendPort,keyword,url);
			}
			else if (requestType.equals("UPDATE_INDEX")){
				String url=jsonObj.get("url").toString();
				String keyword=jsonObj.get("keyword").toString();
				node.word=keyword;
				node.url.add(url);
			}

			else if(requestType.equals("SEARCH")) {

				String word=jsonObj.get("keyword").toString();
				int sendPort=jsonObj.getInt("sendPort");
				//System.out.println("Node searched " + node.node_id);

				handleSearch(word,sendPort);

			}
			else if(requestType.equals("SEARCH_RESPONSE")){

				int port=(int) jsonObj.get("portSearching");
				sendResults(port,node.url.toString());
			}
			else if(requestType.equals("RESULTS")) {

				String result=jsonObj.getString("results").toString();
				String res= result.substring(1,result.length()-1);
				String [] results=res.split(",");

				for(int i=0;i<results.length;i++){

					node.SearchResults.add(results[i]);
				}
			}

			else if(requestType.equals("LEAVING_NETWORK")) {}




		}catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}



	void handleJoin(int sendPort) throws JSONException, IOException{

		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put("type", "UPDATE_TABLE");
		jsonMessage.put("node_id",node.routing_Table.keySet());
		jsonMessage.put("port",node.routing_Table.values() );


		String JsonMessageString=jsonMessage.toString() ;

		@SuppressWarnings("resource")
		DatagramSocket s = new DatagramSocket();
		InetAddress local =InetAddress.getByName("localhost");
		int msg_length = JsonMessageString.length();
		byte[] message = JsonMessageString.getBytes();
		DatagramPacket p = new DatagramPacket(message, msg_length, local,sendPort);
		s.send(p);
	}
	void handleIndexing(int port, String keyword, String url) throws JSONException, IOException{

		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put("type","UPDATE_INDEX");
		jsonMessage.put("url",url);
		jsonMessage.put("keyword",keyword);

		String JsonMessageString=jsonMessage.toString() ;

		@SuppressWarnings("resource")
		DatagramSocket s = new DatagramSocket();
		InetAddress local =InetAddress.getByName("localhost");
		int msg_length = JsonMessageString.length();
		byte[] message = JsonMessageString.getBytes();
		DatagramPacket p = new DatagramPacket(message, msg_length,local,port);
		s.send(p);




	}
	void handleSearch(String keyword,int port ) throws JSONException, IOException{

		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put("type","SEARCH_RESPONSE");
		jsonMessage.put("keyword",keyword);
		jsonMessage.put("portSearching",node.port);

		String JsonMessageString=jsonMessage.toString() ;

		@SuppressWarnings("resource")
		DatagramSocket s = new DatagramSocket();
		InetAddress local =InetAddress.getByName("localhost");
		int msg_length = JsonMessageString.length();
		byte[] message = JsonMessageString.getBytes();
		DatagramPacket p = new DatagramPacket(message, msg_length,local,port);
		s.send(p);

	}
	void sendResults(int port,String results)throws JSONException, IOException{
		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put("type", "RESULTS");
		jsonMessage.put("results",results);

		String JsonMessageString=jsonMessage.toString() ;

		@SuppressWarnings("resource")
		DatagramSocket s = new DatagramSocket();
		InetAddress local =InetAddress.getByName("localhost");
		int msg_length = JsonMessageString.length();
		byte[] message = JsonMessageString.getBytes();
		DatagramPacket p = new DatagramPacket(message, msg_length,local,port);
		s.send(p);

	}


}

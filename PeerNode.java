import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.*;


public class PeerNode  implements Runnable{

	String word;
	ArrayList<String> url = new ArrayList<String>();
	ArrayList<String> SearchResults = new ArrayList<String>();
	int port,node_id;
	Map <Integer, Integer> routing_Table = new HashMap<Integer, Integer> ();


	public PeerNode(){}
	public PeerNode(String id){
		this.node_id=hashCode(id);

	}
	public Map<Integer, Integer> getRouting_Table() {
		return routing_Table;
	}
	public void setRouting_Table(Map<Integer, Integer> routing_Table) {
		this.routing_Table = routing_Table;
	}
	public int  getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public void init (int port){
		this.port=port;
		this.routing_Table.put(this.node_id,port);
		new Thread(this).start();
	}
	public void joinNetwork(InetSocketAddress iPort,String id,String identifier ) throws JSONException, IOException{

		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put("type", "JOINING_NETWORK_SIMPLIFIED");
		//id of Sender
		jsonMessage.put("node_id", hashCode(id));
		//id of target
		jsonMessage.put("target_id",hashCode(identifier));
		//ip address
		jsonMessage.put("ip_address",iPort.getAddress());
		//the port of the sender
		jsonMessage.put("sending_port",this.port);
		String JsonMessageString=jsonMessage.toString() ;

		@SuppressWarnings("resource")
		DatagramSocket s = new DatagramSocket();
		InetAddress local =iPort.getAddress();
		int msg_length = JsonMessageString.length();
		byte[] message = JsonMessageString.getBytes();

		DatagramPacket p = new DatagramPacket(message, msg_length, local,iPort.getPort());
		s.send(p);

	}
	public static void leaveNetwork(PeerNode node )throws JSONException, IOException{
		node.node_id =0;
		node.port=0;
		node.routing_Table=null;
		node.SearchResults=null;
		node.url=null;
		node.word=null;
	}
	void indexPage(String url, String word) throws JSONException, IOException{

		int port = 0;
		if (this.routing_Table.containsKey(hashCode(word))){
			port=this.routing_Table.get(hashCode(word));

		}else{
			for (Integer value : this.routing_Table.values()) {
				if(this.port!=value){
					port=value;
				}
			}
		}	
		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put("type", "INDEX");
		jsonMessage.put("node_id", this.node_id);
		jsonMessage.put("target_id", hashCode(word));
		jsonMessage.put("keyword", word);
		jsonMessage.put("link", url);
		jsonMessage.put("sendPort", port);

		String JsonMessageString=jsonMessage.toString() ;

		@SuppressWarnings("resource")
		DatagramSocket s = new DatagramSocket();
		InetAddress local = InetAddress.getByName("localhost");
		int msg_length = JsonMessageString.length();
		byte[] message = JsonMessageString.getBytes();
		DatagramPacket p = new DatagramPacket(message, msg_length, local,this.port);
		s.send(p);

	}
	void search(String word) throws JSONException, IOException{

		int port = 0;
		if (this.routing_Table.containsKey(hashCode(word))){
			port=this.routing_Table.get(hashCode(word));

		}else{
			for (Integer value : this.routing_Table.values()) {
				if(this.port!=value){
					port=value;
				}
			}
		}

		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put("type", "SEARCH");
		jsonMessage.put("keyword", word);
		jsonMessage.put("node_id", this.node_id);
		jsonMessage.put("target_id", hashCode(word));
		jsonMessage.put("sendPort", port);

		String JsonMessageString=jsonMessage.toString() ;

		@SuppressWarnings("resource")
		DatagramSocket s = new DatagramSocket();
		InetAddress local = InetAddress.getByName("localhost");
		int msg_length = JsonMessageString.length();
		byte[] message = JsonMessageString.getBytes();
		DatagramPacket p = new DatagramPacket(message, msg_length, local,this.port);
		s.send(p);
	}
	public static int hashCode(String str) {
		int hash = 0;
		for (int i = 0; i < str.length(); i++) {
			hash = hash * 31 + str.charAt(i);
		}
		return Math.abs(hash);
	}
	public void run() {
		DatagramSocket s = null;
		while(true){
			byte[] message = new byte[1024];
			DatagramPacket p = new DatagramPacket(message, message.length);

			try {
				s = new DatagramSocket(this.port);
				s.receive(p);
				RequestHandler request=new RequestHandler(p,this);
				request.run();
			} catch (Exception e) {
				e.printStackTrace();
			}

			s.close();
		}

	}
}

















/*
public void sendRequest(String sentence) throws UnknownHostException, IOException{
	@SuppressWarnings("resource")
	DatagramSocket s = new DatagramSocket();
	InetAddress local = InetAddress.getByName("localhost");
	int msg_length =  sentence.length();
	byte[] message =  sentence.getBytes();
	DatagramPacket p = new DatagramPacket(message, msg_length, local,8767);
	System.out.println("about to send msg1");
	s.send(p);
}*/
/*
new Thread(new Runnable() {
	   public void run() {
		   try {
			processRequest(p);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }
	}).start();



				//
				//t.run();

				/*

 * workerThreads = new Thread[10];
		        for (Thread t : workerThreads) {

		            server.start();
		        }

				//
				//service.submit(server);

				//executor.execute(worker);
				//new Thread(server).start();


 */


/*	
	void mainLoopServer(int port) throws IOException{
		//try {
			// Establish the listen socket.
			DatagramSocket serverSocket = new DatagramSocket(port);   
			byte[] receiveData = new byte[1024];
			byte[] sendData = new byte[1024]; 
			System.out.println("listening on port " + serverSocket.getLocalPort());

			while (true) {
				System.out.println("Server:  ");
			}

				/*
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				serverSocket.receive(receivePacket);

				String sentence = new String( receivePacket.getData());
				System.out.println("Server:  "+sentence);
				/*RequestHandler request =new RequestHandler (receivePacket,serverSocket);

				// Create a new thread to process the request.
				Thread thread = new Thread(request);

				// Start the thread.
				thread.start(); 
				count++;
				/////////
				String capitalizedSentence = sentence.toUpperCase();                   
				sendData = capitalizedSentence.getBytes();                   
				DatagramPacket sendPacket =new DatagramPacket(sendData, sendData.length,9888);
				serverSocket.send(sendPacket);  

			}     
		}catch (Exception e) {	// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} 
	}
	void mainLoopClient(int port) throws IOException{
		try {
			// Establish the listen socket.
			DatagramSocket serverSocket = new DatagramSocket(port);   
			byte[] receiveData = new byte[1024]; 
			System.out.println("listening on port " + serverSocket.getLocalPort());

			while (true) {

				System.out.println("packet recieved");

				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				serverSocket.receive(receivePacket);

				String sentence = new String( receivePacket.getData());
				System.out.println("Server:  "+sentence);
				RequestHandler request =new RequestHandler (receivePacket,serverSocket);

				// Create a new thread to process the request.
				Thread thread = new Thread(request);

				// Start the thread.
				thread.start(); 

			}     

		}catch (Exception e) {	// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} 
 */	

/*
PeerNode(int port ) throws IOException{
	mainLoop(port);
}	

	public void sendRequest(String sentence, String host, int port) throws UnknownHostException, IOException{

		clientSocket = new DatagramSocket (port);
		InetAddress IPAddress = InetAddress.getByName(host);   
		byte[] sendData = new byte[1024];       
		sendData = sentence.getBytes();        
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port); 
		clientSocket.send(sendPacket);
		System.out.println("Packet sent");
	}


	public static boolean available(int port) {
	    System.out.println("--------------Testing port " + port);
	    DatagramSocket s = null;
	    try {
	        s = new DatagramSocket(port);

	        // If the code makes it this far without an exception it means
	        // something is using the port and has responded.
	        System.out.println("--------------Port " + port + " is not available");
	        return false;
	    } catch (IOException e) {
	        System.out.println("--------------Port " + port + " is available");
	        return true;
	    } finally {
	        if( s != null){
	        }
	    }
	}

	void mainLoop(int port) throws IOException{
		try {
			// Establish the listen socket.
			DatagramSocket serverSocket = new DatagramSocket(port);   
			byte[] receiveData = new byte[1024]; 
			System.out.println("listening on port " + serverSocket.getLocalPort());
			int count =0;
			while (count==0) {
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				serverSocket.receive(receivePacket);
				System.out.println("packet recieved");
				String sentence = new String( receivePacket.getData());
				System.out.println("Server:  "+sentence);
				RequestHandler request =new RequestHandler (receivePacket,serverSocket);

				// Create a new thread to process the request.
				Thread thread = new Thread(request);

				// Start the thread.
				thread.start(); 
				count++;
			}     
		}catch (Exception e) {	// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}



 */



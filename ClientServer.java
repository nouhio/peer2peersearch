import java.io.IOException;
import java.net.InetSocketAddress;

import org.json.*;

public class ClientServer {

	public static void main( String [] args ) throws IOException, JSONException{


		System.out.println("--------------------START---------------------");
		PeerNode bootStrap= new PeerNode("bootstrap");
		bootStrap.init(8767);
		
		System.out.println("--------------------Create few Nodes---------------------");
		
		System.out.println("boot ID "+ bootStrap.node_id);
		PeerNode node1= new PeerNode("test");
		node1.init(8768);

		System.out.println("Node1 ID "+node1.node_id);

		PeerNode node2= new PeerNode("best");
		node2.init(8769);
		System.out.println("Node2 ID "+node2.node_id);

		PeerNode node3= new PeerNode("tcd");
		node3.init(8770);
		System.out.println("Node3 ID "+node3.node_id);

		System.out.println("--------------------Node 1 Joins at  Bootstrap---------------------");

		InetSocketAddress node1join=new InetSocketAddress("localhost",bootStrap.getPort());
		node1.joinNetwork(node1join, "test", "bootStrap");
		
		pause();

		System.out.println("--------------------Node 2 Joins at Node 1---------------------");
		InetSocketAddress node2join=new InetSocketAddress("localhost",node1.getPort());
		node2.joinNetwork(node2join, "best", "test");

		pause();

		System.out.println("--------------------Node 3 Joins at Bootstrap---------------------");
		InetSocketAddress node3join=new InetSocketAddress("localhost",bootStrap.getPort());
		node3.joinNetwork(node3join, "tcd", "bootstarp");

		pause();

		System.out.println("--------------------Printing the routing tables (NodeOd/Port)---------------------");
		System.out.println("node 1 "+node1.getRouting_Table().toString());

		System.out.println("node 2 "+node2.getRouting_Table().toString());

		System.out.println("node 3 "+node3.getRouting_Table().toString());

		System.out.println("bootstrap "+bootStrap.getRouting_Table().toString());

		pause();

		System.out.println("--------------------Node 3 Routes URL (www.tcd.ie) to Node1 that index (keyword test) ---------------------");
		
		node3.indexPage("www.tcd.ie", "test");


		pause();

		System.out.println("--------------------Node 3 Routes URL (www.tcc.ie) to Node1 that index (keyword test) ---------------------");
		node3.indexPage("www.tcc.ie", "test");

		pause();
		pause();
		
		System.out.println("--------------------Node 2 Search for (keyword test) Node 1 ---------------------");
		
		node2.search("test");
		pause();

		System.out.println("--------------------Node 2 Print the results of the search ---------------------");
		System.out.println("Node2 gets the results : "+node2.SearchResults.toString());
		
		pause();
		
		
		System.out.println("--------------------Node 2 Leaves the Network ---------------------");
		PeerNode.leaveNetwork(node2);
		
		
		System.out.println("--------------------Try to access Node 2 throws a null exception ---------------------");
		try{
		System.out.print(node2.SearchResults.toString());
		}catch(Exception e){
			System.out.print("Node left the Network :: " +e);
		}
		

	}

	public static void pause(){
		for(long i=0;i<1000000000;i++);
		for(long i=0;i<1000000000;i++);
		for(long i=0;i<1000000000;i++);
		for(long i=0;i<1000000000;i++);
	}



}

